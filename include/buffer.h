/**
 * @file buffer.h
 * @brief Buffer define and functions.
 * @copyright Copyright (C) 2023 ChenPi11.
 * @date 2023-12-30
 * @version 0.1.0
 * @note This file is part of the charset experiment.
 * @link https://gitlab.com/ChenPi11/source-code-of-the-experiment
 */

/*
 * Copyright (C) 2023 ChenPi11.
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Buffer define and functions.
 */

#pragma once

#include <stddef.h>
#include <stdint.h>

/**
 * @brief Buffer type.
 */
struct buffer_t
{
    /**
     * @brief Buffer data.
     */
    uint8_t* data;

    /**
     * @brief Buffer size.
     */
    size_t size;
};

/**
 * @brief Allocate a new buffer.
 * @param size Buffer size you want to allocate.
 * @return The new buffer.
 */
struct buffer_t alloc_buffer(size_t size);

/**
 * @brief Free buffer.
 * @param buffer The buffer you want to free.
 */
void free_buffer(struct buffer_t* buffer);

/**
 * @brief Initialize buffer with a string.
 * @param string The string you want to initialize buffer.
 */
struct buffer_t buffer_from_string(const char* string);

/**
 * @brief Print buffer.
 * @param buffer The buffer you want to print.
 */
void print_buffer(struct buffer_t buffer);
