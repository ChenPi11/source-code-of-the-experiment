/**
 * @file tcpex.h
 * @brief TCPEX header file.
 * @copyright Copyright (C) 2023 ChenPi11.
 * @date 2023-12-30
 * @version 0.1.0
 * @note This file is part of the charset experiment.
 * @link https://gitlab.com/ChenPi11/source-code-of-the-experiment
 */

/*
 * Copyright (C) 2023 ChenPi11.
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * TCPEX functions.
 */

#pragma once

#include <stdint.h>

#include <buffer.h>
#include <tcp4.h>

/**
 * @brief TCPEX data header.
 */
struct tcpex_header
{
    /**
     * @brief Data size
     */
    uint64_t size;
};

/**
 * @brief Send all data to a TCPEX socket.
 * @param sock TCPEX socket.
 * @param buffer Buffer.
 */
void sendall(tcp4sock_t sock, struct buffer_t buffer);

/**
 * @brief Receive all data from a TCPEX socket.
 * @param sock TCPEX socket.
 * @param size Data size you want to receive.
 * @return Buffer.
 */
struct buffer_t recvall(tcp4sock_t sock, size_t size);

/**
 * @brief Send data to a TCPEX socket.
 * @param sock TCPEX socket.
 * @param buffer Buffer.
 */
void tcpex_send(tcp4sock_t sock, struct buffer_t buffer);

/**
 * @brief Receive data from a TCPEX socket.
 * @param sock TCPEX socket.
 * @return Buffer.
 */
struct buffer_t tcpex_recv(tcp4sock_t sock);

/**
 * @brief Close a TCPEX socket.
 * @param sock TCPEX socket.
 */
void tcpex_close(tcp4sock_t sock);
