/**
 * @file tcp4.h
 * @brief IPv4 TCP socket functions.
 * @copyright Copyright (C) 2023 ChenPi11.
 * @date 2023-12-30
 * @version 0.1.0
 * @note This file is part of the charset experiment.
 * @link https://gitlab.com/ChenPi11/source-code-of-the-experiment
 */

/*
 * Copyright (C) 2023 ChenPi11.
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * IPv4 TCP socket functions.
 */

#pragma once

#include <buffer.h>

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

#include <cppp/cppp-platform.h>

#if __has_windows__
#ifdef _WIN64
typedef uint64_t tcp4sock_t;
typedef int64_t ssize_t;
#else
typedef uint32_t tcp4sock_t;
typedef int32_t ssize_t;
#endif
#else
typedef int tcp4sock_t;
#endif

/**
 * @brief IPv4 TCP address.
 */
struct tcp4_addr
{
    /**
     * @brief The IP address.
     */
    const char* ip;
    /**
     * @brief The port.
     */
    uint16_t port;
};

/**
 * @brief Initialize TCP4.
 * @return The result.
 */
int init_tcp4();

/**
 * @brief Create a IPv4 TCP socket.
 * @return The socket.
 */
tcp4sock_t create_tcp4();

/**
 * @brief Bind a IPv4 TCP socket.
 * @param sock Socket.
 * @param addt Address.
 */
void bind_tcp4(tcp4sock_t sock, struct tcp4_addr addr);

/**
 * @brief Listen a IPv4 TCP socket.
 */
void listen_tcp4(tcp4sock_t sock, int backlog);

/**
 * @brief Accept a IPv4 TCP socket.
 * @param sock Socket.
 * @return The accepted socket.
 */
tcp4sock_t accept_tcp4(tcp4sock_t sock);

/**
 * @brief Connect a IPv4 TCP socket.
 * @param sock Socket.
 * @param addt Address.
 */
void connect_tcp4(tcp4sock_t sock, struct tcp4_addr addr);

/**
 * @brief Send data to a IPv4 TCP socket.
 * @param sock Socket.
 * @param buffer Buffer.
 */
ssize_t tcp4_send(tcp4sock_t sock, struct buffer_t buffer);

/**
 * @brief Receive data from a IPv4 TCP socket.
 * @param sock Socket.
 * @param size Size.
 */
struct buffer_t tcp4_recv(tcp4sock_t sock, size_t size);

/**
 * @brief Shutdown a IPv4 TCP socket.
 */
void shutdown_tcp4(tcp4sock_t sock);

/**
 * @brief Close a IPv4 TCP socket.
 * @param sock Socket.
 */
void close_tcp4(tcp4sock_t sock);
