/**
 * @file cppp/cppp-platform.h
 * @author ChenPi11
 * @copyright Copyright (C) 2023 The C++ Plus Project
 * @date 2023-11-19
 * @brief C++ Plus platform predefined detection.
 * @version 1.3.0
 * @see https://github.com/cpredef/predef
 * @link https://github.com/cppp-project/cppp-platform
 */
/* Copyright (C) 2023 The C++ Plus Project
   This file is part of the cppp-platform library.

   The cppp-platform library is free software; you can redistribute it
   and/or modify it under the terms of the The Unlicense as published
   by the unlicense.org

   The cppp-platform library is distributed in the hope that it will be
   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the The
   Unlicense for more details.

   You should have received a copy of the The Unlicense along with the
   cppp-platform library; see the file COPYING.
   If not, see <http://unlicense.org/>.  */

#ifndef _CPPP_PLATFORM_H_INCLUDED
#define _CPPP_PLATFORM_H_INCLUDED 1
/* Architectures detection. */
/* Alpha */
#define _CPPP_PLATFORM_PLATFORMID_ALPHA 1
/* x86 */
#define _CPPP_PLATFORM_PLATFORMID_X86 2
/* ARM */
#define _CPPP_PLATFORM_PLATFORMID_ARM 3
/* Blackfin */
#define _CPPP_PLATFORM_PLATFORMID_BLACKFIN 4
/* Convex */
#define __arch_convex__ 5
/* Epiphany */
#define __arch_epiphany__ 6
/* HP/PA RISC */
#define __arch_hppa__ 7
/* Itanium */
#define __arch_itanium__ 8
/* Motorola 68k */
#define __arch_motorola68k__ 9
/* MIPS */
#define __arch_mips__ 10
/* PowerPC */
#define __arch_ppc__ 11
/* Pyramid 9810 */
#define __arch_pyramid9810__ 12
/* RS/6000 */
#define __arch_rs6000__ 13
/* SPARC */
#define __arch_sparc__ 14
/* SuperH */
#define __arch_superh__ 15
/* SystemZ */
#define __arch_systemz__ 16
/* TMS320 */
#define __arch_tms320__ 17
/* TMS470 */
#define __arch_tms470__ 18
/* Unknown architectures */
#define __arch_unknown__ 0

#define __arch__ __arch_unknown__
#define __arch_name__ "unknown"
#if defined(__alpha__) || defined(__alpha) || defined(_M_ALPHA)
#undef __arch__
#undef __arch_name__
#define __arch__ _CPPP_PLATFORM_PLATFORMID_ALPHA
#define __arch_name__ "Alpha"
#endif
#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64) || defined(_M_X64) || defined(_M_AMD64) 
#undef __arch__
#undef __arch_name__
#define __arch__ _CPPP_PLATFORM_PLATFORMID_X86
#define __arch_name__ "AMD64"
#define __lp_amd64_flag 1
#endif
#if defined(__aarch64__)
#undef __arch__
#undef __arch_name__
#define __arch__ _CPPP_PLATFORM_PLATFORMID_ARM
#define __arch_name__ "aarch64"
#endif
#if defined(__arm__) || defined(__thumb__) || defined(_ARM) || defined(_M_ARMT) || defined(__arm)
#undef __arch__
#undef __arch_name__
#define __arch__ _CPPP_PLATFORM_PLATFORMID_ARM
#define __arch_name__ "ARM"
#endif
#if defined(__bfin) || defined(__BFIN__)
#undef __arch__
#undef __arch_name__
#define __arch__ _CPPP_PLATFORM_PLATFORMID_BLACKFIN
#define __arch_name__ "Blackfin"
#endif
#if defined(__convex__)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_convex__
#define __arch_name__ "Convex"
#endif
#if defined(__epiphany__)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_epiphany__
#define __arch_name__ "Epiphany"
#endif
#if defined(__hppa__) || defined(__HPPA__) || defined(__hppa)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_hppa__
#define __arch_name__ "HP/PA RISC"
#endif
#if (defined(i386) || defined(__i386) || defined(__i386__) || defined(__i486__) || defined(__i586__) || defined(__i686__) || defined(__i386) || defined(__IA32__) || defined(_M_I86) || defined(_M_IX86) || defined(__X86__) || defined(_X86_) || defined(__THW_INTEL__) || defined(__I86__) || defined(__INTEL__) || defined(__386)) && !defined(__lp_amd64_flag)
#undef __arch__
#undef __arch_name__
#define __arch__ _CPPP_PLATFORM_PLATFORMID_X86
#define __arch_name__ "Intel x86"
#endif
#if defined(__ia64__) || defined(_IA64) || defined(__IA64__) || defined(__ia64) || defined(_M_IA64) || defined(__itanium__)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_itanium__
#define __arch_name__ "Intel Itanium"
#endif
#if defined(__m68k__) || defined(M68000) || defined(__MC68K__)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_motorola68k__
#define __arch_name__ "Motorola 68k"
#endif
#if defined(__mips__) || defined(mips) || defined(_MIPS_ISA) || defined(__mips) || defined(__MIPS__)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_mips__
#define __arch_name__ "MIPS"
#endif
#if defined(__powerpc) || defined(__powerpc__) || defined(__powerpc64__) || defined(__POWERPC__) || defined(__ppc__) || defined(__ppc64__) || defined(__PPC__) || defined(__PPC64__) || defined(_ARCH_PPC) || defined(_ARCH_PPC64) || defined(_M_PPC) || defined(__ppc)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_ppc__
#define __arch_name__ "PowerPC"
#endif
#if defined(pyr)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_pyramid9810__
#define __arch_name__ "Pyramid 9810"
#endif
#if defined(__THW_RS6000) || defined(_IBMR2) || defined(_POWER) || defined(_ARCH_PWR) || defined(_ARCH_PWR2) || defined(_ARCH_PWR3) || defined(_ARCH_PWR4)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_rs6000__
#define __arch__ "RS/6000"
#endif
#if defined(__sparc__) || defined(__sparc)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_sparc__
#define __arch_name__ "SPARC"
#endif
#if defined(__sh__)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_superh__
#define __arch_name__ "SuperH"
#endif
#if defined(__370__) || defined(__THW_370__) || defined(__s390__) || defined(__s390x__) || defined(__zarch__) || defined(__zarch__) || defined(__SYSC_ZARCH__)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_systemz__
#define __arch__ "SystemZ"
#endif
#if defined(_TMS320C2XX) || defined(__TMS320C2000__) || defined(_TMS320C5X) || defined(__TMS320C55X__) || defined(_TMS320C6X) || defined(__TMS320C6X__)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_tms320__
#define __arch_name__ "TMS320"
#endif
#if defined(__TMS470__)
#undef __arch__
#undef __arch_name__
#define __arch__ __arch_tms470__
#define __arch_name__ "TMS470"
#endif

#ifndef __POINTER_WIDTH__
/* Pointer width 64/32/16... */
#define __POINTER_WIDTH__ (long)(sizeof(void*) * 8)
#endif
/* Fix amd64 and 8086 bug */
#undef __lp_amd64_flag


/* Compilers detection. */
/* ACC */
#define __has_acc_compiler__ 0
/* Altium MicroBlaze C */
#define __has_has_altium_microblaze_c_compiler__ 0
/* Altium C-to-Hardware */
#define __has_altium_c_to_hardware_compiler__ 0
/* Amsterdam Compiler Kit */
#define __has_amsterdam_compiler__ 0
/* ARM Compiler */
#define __has_arm_compiler__ 0
/* Aztec C */
#define __has_aztec_c_compiler__ 0
/* Borland C/C++ */
#define __has_borland_compiler__ 0
/* CC65 */
#define __has_cc65_compiler__ 0
/* Clang */
#define __has_clang_compiler__ 0
/* Comeau */
#define __has_comeau_compiler__ 0
/* Compaq C/C++ */
#define __has_compaq_compiler__ 0
/* Convex C */
#define __has_convex_c_compiler__ 0
/* CompCert */
#define __has_compcert_compiler__ 0
/* Coverity C/C++ Static Analyzer */
#define __has_coverity_compiler__ 0
/* Cray C */
#define __has_cray_c_compiler__ 0
/* Diab C/C++ */
#define __has_diab_compiler__ 0
/* DICE C */
#define __has_dice_c_compiler__ 0
/* Digital Mars */
#define __has_digital_mars_compiler__ 0
/* Dignus Systems/C++ */
#define __has_dignus_systems_compiler__ 0
/* DJGPP */
#define __has_djgpp_compiler__ 0
/* EDG C++ Frontend */
#define __has_edg_compiler__ 0
/* EKOPath */
#define __has_ekopath_compiler__ 0
/* Fujitsu C++ */
#define __has_fujitsu_compiler__ 0
/* GCC C/C++ */
#define __has_gcc_compiler__ 0
/* Green Hill C/C++ */
#define __has_greenhill_compiler__ 0
/* HP ANSI C */
#define __has_hpansi_c_compiler__ 0
/* HP aC++ */
#define __has_hpa_compiler__ 0
/* IAR C/C++ */
#define __has_iar_compiler__ 0
/* ImageCraft C */
#define __has_imagecraft_c_compiler__ 0
/* Intel C/C++ */
#define __has_intel_compiler__ 0
/* KAI C++ */
#define __has_kai_compiler__ 0
/* KEIL CARM */
#define __has_keil_carm_compiler__ 0
/* KEIL C166 */
#define __has_keil_c166_compiler__ 0
/* KEIL C51 */
#define __has_keil_c51_compiler__ 0
/* LCC */
#define __has_lcc_compiler__ 0
/* LLVM */
#define __has_llvm_compiler__ 0
/* MetaWare High C/C++ */
#define __has_metaware_high_compiler__ 0
/* Metrowerks CodeWarrior */
#define __has_metrowerks_codewarrior_compiler__ 0
/* Microsoft Visual C++ */
#define __has_msvc_compiler__ 0
/* Microtec C/C++ */
#define __has_microtec_compiler__ 0
/* Microway NDP C */
#define __has_microway_ndp_c_compiler__ 0
/* MinGW */
#define __has_mingw_compiler__ 0
/* MIPSpro */
#define __has_mipspro_compiler__ 0
/* Miracle C */
#define __has_miracle_c_compiler__ 0
/* MPW C++ */
#define __has_mpr_compiler__ 0
/* Norcroft C */
#define __has_norcroft_c_compiler__ 0
/* NWCC */
#define __has_nwcc_compiler__ 0
/* Open64 */
#define __has_open64_compiler__ 0
/* Oracle Pro*C Precompiler */
#define __has_oracle_pro_compiler__ 0
/* Oracle Solaris Studio */
#define __has_oracle_solaris_studio_compiler__ 0
/* Pacific C */
#define __has_pacific_c_compiler__ 0
/* Palm C/C++ */
#define __has_palm_compiler__ 0
/* Pelles C */
#define __has_pelles_c_compiler__ 0
/* Portland Group C/C++ */
#define __has_portland_group_compiler__ 0
/* Renesas C/C++ */
#define __has_renesas_compiler__ 0
/* SAS/C */
#define __has_sas_c_compiler__ 0
/* SCO OpenServer */
#define __has_sco_compiler__ 0
/* Small Device C Compiler */
#define __has_small_device_compiler__ 0
/* SN Compiler */
#define __has_sn_compiler__ 0
/* Stratus VOS C */
#define __has_stratus_vos_c_compiler__ 0
/* Symantec C++ */
#define __has_symantec_compiler__ 0
/* TenDRA C/C++ */
#define __has_tendra_compiler__ 0
/* Texas Instruments C/C++ Compiler */
#define __has_texas_instruments_compiler__ 0
/* THINK C */
#define __has_think_c_compiler__ 0
/* Tiny C */
#define __has_tiny_c_compiler__ 0
/* Turbo C/C++ */
#define __has_turboc_compiler__ 0
/* Ultimate C/C++ */
#define __has_ultimate_compiler__ 0
/* USL C */
#define __has_usl_c_compiler__ 0
/* VBCC */
#define __has_vbcc_compiler__ 0
/* Watcom C++ */
#define __has_watcom_compiler__ 0
/* Zortech C++ */
#define __has_zortech_compiler__ 0

#if defined(_ACC_)
#undef __has_acc_compiler__
/* ACC */
#define __has_acc_compiler__ 1
#endif
#if defined(__CMB__)
#undef __has_has_altium_microblaze_c_compiler__
/* Altium MicroBlaze C */
#define __has_has_altium_microblaze_c_compiler__ 1
#endif
#if defined(__CHC__)
#undef __has_altium_c_to_hardware_compiler__
/* Altium C-to-Hardware */
#define __has_altium_c_to_hardware_compiler__ 1
#endif
#if defined(__ACK__)
#undef __has_amsterdam_compiler__
/* Amsterdam Compiler Kit */
#define __has_amsterdam_compiler__ 1
#endif
#if defined(__CC_ARM)
#undef __has_arm_compiler__
/* ARM Compiler */
#define __has_arm_compiler__ 1
#endif
#if defined(AZTEC_C)
#undef __has_aztec_c_compiler__
/* Aztec C */
#define __has_aztec_c_compiler__ 1
#endif
#if defined(__BORLANDC__)
#undef __has_borland_compiler__
/* Borland C/C++ */
#define __has_borland_compiler__ 1
#endif
#if defined(__CC65__)
#undef __has_cc65_compiler__
/* CC65 */
#define __has_cc65_compiler__ 1
#endif
#if defined(__clang__)
#undef __has_clang_compiler__
/* Clang */
#define __has_clang_compiler__ 1
#endif
#if defined(__COMO__)
#undef __has_comeau_compiler__
/* Comeau */
#define __has_comeau_compiler__ 1
#endif
#if defined(__DECC)
#undef __has_compaq_compiler__
/* Compaq C/C++ */
#define __has_compaq_compiler__ 1
#endif
#if defined(__convexc__)
#undef __has_convex_c_compiler__
/* Convex C */
#define __has_convex_c_compiler__ 1
#endif
#if defined(__COMPCERT__)
#undef __has_compcert_compiler__
/* CompCert */
#define __has_compcert_compiler__ 1
#endif
#if defined(__COVERITY__)
#undef __has_coverity_compiler__
/* Coverity C/C++ Static Analyzer */
#define __has_coverity_compiler__ 1
#endif
#if defined(_CRAYC)
#undef __has_cray_c_compiler__
/* Cray C */
#define __has_cray_c_compiler__ 1
#endif
#if defined(__DCC__)
#undef __has_diab_compiler__
/* Diab C/C++ */
#define __has_diab_compiler__ 1
#endif
#if defined(_DICE)
#undef __has_dice_c_compiler__
/* DICE C */
#define __has_dice_c_compiler__ 1
#endif
#if defined(__DMC__)
#undef __has_digital_mars_compiler__
/* Digital Mars */
#define __has_digital_mars_compiler__ 1
#endif
#if defined(__SYSC__)
#undef __has_dignus_systems_compiler__
/* Dignus Systems/C++ */
#define __has_dignus_systems_compiler__ 1
#endif
#if defined(__DJGPP__)
#undef __has_djgpp_compiler__
/* DJGPP */
#define __has_djgpp_compiler__ 1
#endif
#if defined(__EDG__)
#undef __has_edg_compiler__
/* EDG C++ Frontend */
#define __has_edg_compiler__ 1
#endif
#if defined(__PATHCC__)
#undef __has_ekopath_compiler__
/* EKOPath */
#define __has_ekopath_compiler__ 1
#endif
#if defined(__FCC_VERSION)
#undef __has_fujitsu_compiler__
/* Fujitsu C++ */
#define __has_fujitsu_compiler__ 1
#endif
#if defined(__GNUC__)
#undef __has_gcc_compiler__
/* GCC C/C++ */
#define __has_gcc_compiler__ 1
#endif
#if defined(__ghs__)
#undef __has_greenhill_compiler__
/* Green Hill C/C++ */
#define __has_greenhill_compiler__ 1
#endif
#if defined(__HP_cc)
#undef __has_hpansi_c_compiler__
/* HP ANSI C */
#define __has_hpansi_c_compiler__ 1
#endif
#if defined(__HP_aCC)
#undef __has_hpa_compiler__
/* HP aC++ */
#define __has_hpa_compiler__ 1
#endif
#if defined(__IAR_SYSTEMS_ICC__)
#undef __has_iar_compiler__
/* IAR C/C++ */
#define __has_iar_compiler__ 1
#endif
#if defined(__IMAGECRAFT__)
#undef __has_imagecraft_c_compiler__
/* ImageCraft C */
#define __has_imagecraft_c_compiler__ 1
#endif
#if defined(__INTEL_COMPILER)
#undef __has_intel_compiler__
/* Intel C/C++ */
#define __has_intel_compiler__ 1
#endif
#if defined(__KCC)
#undef __has_kai_compiler__
/* KAI C++ */
#define __has_kai_compiler__ 1
#endif
#if defined(__CA__) || defined(__KEIL__)
#undef __has_keil_carm_compiler__
/* KEIL CARM */
#define __has_keil_carm_compiler__ 1
#endif
#if defined(__C166__)
#undef __has_keil_c166_compiler__
/* KEIL C166 */
#define __has_keil_c166_compiler__ 1
#endif
#if defined(__C51__) || defined(__CX51__)
#undef __has_keil_c51_compiler__
/* KEIL C51 */
#define __has_keil_c51_compiler__ 1
#endif
#if defined(__LCC__)
#undef __has_lcc_compiler__
/* LCC */
#define __has_lcc_compiler__ 1
#endif
#if defined(__llvm__)
#undef __has_llvm_compiler__
/* LLVM */
#define __has_llvm_compiler__ 1
#endif
#if defined(__HIGHC__)
#undef __has_metaware_high_compiler__
/* MetaWare High C/C++ */
#define __has_metaware_high_compiler__ 1
#endif
#if defined(__MWERKS__)
#undef __has_metrowerks_codewarrior_compiler__
/* Metrowerks CodeWarrior */
#define __has_metrowerks_codewarrior_compiler__ 1
#endif
#if defined(_MSC_VER)
#undef __has_msvc_compiler__
/* Microsoft Visual C++ */
#define __has_msvc_compiler__ 1
#endif
#if defined(_MRI)
#undef __has_microtec_compiler__
/* Microtec C/C++ */
#define __has_microtec_compiler__ 1
#endif
#if defined(__NDPC__)
#undef __has_microway_ndp_c_compiler__
/* Microway NDP C */
#define __has_microway_ndp_c_compiler__ 1
#endif
#if defined(__MINGW32__) || defined(__MINGW64__)
#undef __has_mingw_compiler__
/* MinGW */
#define __has_mingw_compiler__ 1
#endif
#if defined(__sgi) || defined(sgi)
#undef __has_mipspro_compiler__
/* MIPSpro */
#define __has_mipspro_compiler__ 1
#endif
#if defined(MIRACLE)
#undef __has_miracle_c_compiler__
/* Miracle C */
#define __has_miracle_c_compiler__ 1
#endif
#if defined(__MRC__) || defined(MPW_C) || defined(MPW_CPLUS)
#undef __has_mpr_compiler__
/* MPW C++ */
#define __has_mpr_compiler__ 1
#endif
#if defined(__CC_NORCROFT)
#undef __has_norcroft_c_compiler__
/* Norcroft C */
#define __has_norcroft_c_compiler__ 1
#endif
#if defined(__NWCC__)
#undef __has_nwcc_compiler__
/* NWCC */
#define __has_nwcc_compiler__ 1
#endif
#if defined(__OPEN64__) || defined(__OPENCC__)
#undef __has_open64_compiler__
/* Open64 */
#define __has_open64_compiler__ 1
#endif
#if defined(ORA_PROC)
#undef __has_oracle_pro_compiler__
/* Oracle Pro*C Precompiler */
#define __has_oracle_pro_compiler__ 1
#endif
#if defined(__SUNPRO_C)
#undef __has_oracle_solaris_studio_compiler__
/* Oracle Solaris Studio */
#define __has_oracle_solaris_studio_compiler__ 1
#endif
#if defined(__PACIFIC__)
#undef __has_pacific_c_compiler__
/* Pacific C */
#define __has_pacific_c_compiler__ 1
#endif
#if defined(_PACC_VER)
#undef __has_palm_compiler__
/* Palm C/C++ */
#define __has_palm_compiler__ 1
#endif
#if defined(__POCC__)
#undef __has_pelles_c_compiler__
/* Pelles C */
#define __has_pelles_c_compiler__ 1
#endif
#if defined(__PGI)
#undef __has_portland_group_compiler__
/* Portland Group C/C++ */
#define __has_portland_group_compiler__ 1
#endif
#if defined(__RENESAS__)
#undef __has_renesas_compiler__
/* Renesas C/C++ */
#define __has_renesas_compiler__ 1
#endif
#if defined(SASC) || defined(__SASC) || defined(__SASC__)
#undef __has_sas_c_compiler__
/* SAS/C */
#define __has_sas_c_compiler__ 1
#endif
#if defined(_SCO_DS)
#undef __has_sco_compiler__
/* SCO OpenServer */
#define __has_sco_compiler__ 1
#endif
#if defined(SDCC)
#undef __has_small_device_compiler__
/* Small Device C Compiler */
#define __has_small_device_compiler__ 1
#endif
#if defined(__SNC__)
#undef __has_sn_compiler__
/* SN Compiler */
#define __has_sn_compiler__ 1
#endif
#if defined(__VOSC__)
#undef __has_stratus_vos_c_compiler__
/* Stratus VOS C */
#define __has_stratus_vos_c_compiler__ 1
#endif
#if defined(__SC__)
#undef __has_symantec_compiler__
/* Symantec C++ */
#define __has_symantec_compiler__ 1
#endif
#if defined(__TenDRA__)
#undef __has_tendra_compiler__
/* TenDRA C/C++ */
#define __has_tendra_compiler__ 1
#endif
#if defined(__TI_COMPILER_VERSION__) || defined(_TMS320C6X)
#undef __has_texas_instruments_compiler__
/* Texas Instruments C/C++ Compiler */
#define __has_texas_instruments_compiler__ 1
#endif
#if defined(THINKC3) || defined(THINKC4)
#undef __has_think_c_compiler__
/* THINK C */
#define __has_think_c_compiler__ 1
#endif
#if defined(__TINYC__)
#undef __has_tiny_c_compiler__
/* Tiny C */
#define __has_tiny_c_compiler__ 1
#endif
#if defined(__TURBOC__)
#undef __has_turboc_compiler__
/* Turbo C/C++ */
#define __has_turboc_compiler__ 1
#endif
#if defined(_UCC)
#undef __has_ultimate_compiler__
/* Ultimate C/C++ */
#define __has_ultimate_compiler__ 1
#endif
#if defined(__USLC__)
#undef __has_usl_c_compiler__
/* USL C */
#define __has_usl_c_compiler__ 1
#endif
#if defined(__VBCC__)
#undef __has_vbcc_compiler__
/* VBCC */
#define __has_vbcc_compiler__ 1
#endif
#if defined(__WATCOMC__)
#undef __has_watcom_compiler__
/* Watcom C++ */
#define __has_watcom_compiler__ 1
#endif
#if defined(__ZTC__)
#undef __has_zortech_compiler__
/* Zortech C++ */
#define __has_zortech_compiler__ 1
#endif


/* C/C++ standards detection. */
/* STD C94 */
#define __stdc_c94__ 199409L
/* STD C99 */
#define __stdc_c99__ 199901L
/* STD C11 */
#define __stdc_c11__ 201112L
/* STD C17/C18 */
#define __stdc_c18__ 201710L

#ifdef __STDC_VERSION__
#define __has_c94__ 1
#define __has_c99__ (__STDC_VERSION__ >= __stdc_c99__)
#define __has_c11__ (__STDC_VERSION__ >= __stdc_c11__)
#define __has_c18__ (__STDC_VERSION__ >= __stdc_c18__)
#else
#define __has_c94__ 0
#define __has_c99__ 0
#define __has_c11__ 0
#define __has_c18__ 0
#endif

/* Usually C17 means C18. */
#define __has_c17__ __has_c18__

/* C++98 */
#define __stdcpp_cpp98__ 199711L
/* C++11 */
#define __stdcpp_cpp11__ 201103L
/* C++14 */
#define __stdcpp_cpp14__ 201402L
/* C++17 */
#define __stdcpp_cpp17__ 201703L
/* C++20 */
#define __stdcpp_cpp20__ 202002L

/* C++ predefines */
#ifdef __cplusplus

/* Fix MSVC++ __cplusplus's feature. */
#ifdef _MSVC_LANG
#define __cpp_version__ _MSVC_LANG
#else
#define __cpp_version__ __cplusplus
#endif

/* C++ version */
#define __has_cpp98__ (__cpp_version__ >= __stdcpp_cpp98__)
#define __has_cpp11__ (__cpp_version__ >= __stdcpp_cpp11__)
#define __has_cpp14__ (__cpp_version__ >= __stdcpp_cpp14__)
#define __has_cpp17__ (__cpp_version__ >= __stdcpp_cpp17__)
#define __has_cpp20__ (__cpp_version__ >= __stdcpp_cpp20__)
#else
#define __cpp_version__ 0
#define __has_cpp98__ 0
#define __has_cpp11__ 0
#define __has_cpp14__ 0
#define __has_cpp17__ 0
#define __has_cpp20__ 0
#endif


/* Platforms detection. */
/* No AIX */
#define __has_aix__ 0
/* No Android */
#define __has_android__ 0
/* No Amdahl UTS */
#define __has_amdahl_uts__ 0
/* No AmigaOS */
#define __has_amiga__ 0
/* No Apollo AEGIS */
#define __has_aegis__ 0
/* No Apollo Domain/OS */
#define __has_apollo__ 0
/* No BeOS */
#define __has_beos__ 0
/* No Blue Gene */
#define __has_blue_gene__ 0
/* No BSD */
#define __has_bsd__ 0
/* No ConvexOS */
#define __has_convex__ 0
/* No Cygwin Environment */
#define __has_cygwin__ 0
/* No DG/UX */
#define __has_dgux__ 0
/* No DYNIX/ptx */
#define __has_dynix_ptx__ 0
/* No eCos */
#define __has_ecos__ 0
/* No EMX Environment */
#define __has_emx__ 0
/* No Haiku */
#define __has_haiku__ 0
/* No HI-UX MPP */
#define __has_hiux_mpp__ 0
/* No HP-US */
#define __has_hpux__ 0
/* No IBM OS/400 */
#define __has_os_400__ 0
/* No INTEGRITY */
#define __has_integrity__ 0
/* No Interix Environment */
#define __has_interix__ 0
/* No IRIX */
#define __has_irix__ 0
/* No Linux */
#define __has_linux__ 0
/* No LynxOS */
#define __has_lunx__ 0
/* No MacOS */
#define __has_mac_os__ 0
/* No Microware OS-9 */
#define __has_microware_os_9__ 0
/* No MINIX */
#define __has_minix__ 0
/* No MinGW */
#define __has_mingw__ 0
/* No MorphOS */
#define __has_morph_os__ 0
/* No MPE/iX */
#define __has_mpe_ix__ 0
/* No MSDOS (C++ Plus C unsupport platform) */
#define __has_dos__ 0
/* No NonStop */
#define __has_non_stop__ 0
/* No Nucleus RTOS */
#define __has_nucleus__ 0
/* No OS/2 */
#define __has_os2__ 0
/* No Palm OS */
#define __has_palmos__ 0
/* No EPLAN9 */
#define __has_eplan9__ 0
/* No Pyramid DC/OSx */
#define __has_pyr__ 0
/* No QNX */
#define __has_qnx__ 0
/* No Reliant UNIX */
#define __has_reliant_unix__ 0
/* No sun */
#define __has_sun__ 0
/* No Solaris */
#define __has_solaris__ 0
/* No SunOS */
#define __has_sunos__ 0
/* No Stratus VOS */
#define __has_vos__ 0
/* No SVR4 Environment */
#define __has_svr4__ 0
/* No Syllable */
#define __has_syllable__ 0
/* No Symbian OS */
#define __has_symbianos__ 0
/* No OSF/1 */
#define __has_ofs1__ 0
/* No Ultrix */
#define __has_ultrix__ 0
/* No UNICOS */
#define __has_unicos__ 0
/* No UNICOS/mp */
#define __has_unicos_mp__ 0
/* No UNIX Enviroment */
#define __has_unix__ 0
/* No UnixWare */
#define __has_unix_ware__ 0
/* No U/Win Environment */
#define __has_uwin__ 0
/* No VMS */
#define __has_vms__ 0
/* No VxWorks */
#define __has_vxworks__ 0
/* No Windows */
#define __has_windows__ 0
/* No Windows CE */
#define __has_windows_ce__ 0
/* No Wind/U Environment */
#define __has_windu__ 0
/* No z/OS */
#define __has_zos__ 0

#if defined(_AIX) || defined(__TOS_AIX__)
#undef __has_aix__
/* Has AIX */
#define __has_aix__ 1
#endif
#if defined(_ANDROID_)
#undef __has_android__
/* Has Android */
#define __has_android__ 1
#endif
#if defined(UTS)
#undef __has_amdahl_uts__
/* Has Amdahl UTS */
#define __has_amdahl_uts__ 1
#endif
#if defined(AMIGA) || defined(__amigaos__)
#undef __has_amiga__
/* Has AmigaOS */
#define __has_amiga__ 1
#endif
#if defined(aegis)
#undef __has_aegis__
/* Has Apollo AEGIS */
#define __has_aegis__ 1
#endif
#if defined(apollo)
#undef __has_apollo__
/* Has Apollo Domain/OS */
#define __has_apollo__ 1
#endif
#if defined(__BEOS__)
#undef __has_beos__
/* Has BeOS */
#define __has_beos__ 1
#endif
#if defined(__bg__) || defined(__TOS_BGQ__) || defined(__bgq__) || defined(__THW_BLUEGENE__)
#undef __has_blue_gene__
/* Has Blue Gene */
#define __has_blue_gene__ 1
#endif
#if defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__) || defined(__bsdi__) || defined(__DragonFly__)
#undef __has_bsd__
/* Has BSD */
#define __has_bsd__ 1
#endif
#if defined(__convex__)
#undef __has_convex__
/* Has ConvexOS */
#define __has_convex__ 1
#endif
#if defined(__CYGWIN__)
#undef __has_cygwin__
/* Has Cygwin Environment */
#define __has_cygwin__ 1
#endif
#if defined(DGUX) || defined(__DGUX__) || defined(__dgux__)
#undef __has_dgux__
/* Has DG/UX */
#define __has_dgux__ 1
#endif
#if defined(_SEQUENT_) || defined(sequent)
#undef __has_dynix_ptx__
/* Has DYNIX/ptx */
#define __has_dynix_ptx__ 1
#endif
#if defined(__ECOS)
#undef __has_ecos__
/* Has eCos */
#define __has_ecos__ 1
#endif
#if defined(__EMX__)
#undef __has_emx__
/* Has EMX Environment */
#define __has_emx__ 1
#endif
#if defined(__HAIKU__)
/* Has Haiku */
#define __has_haiku__ 1
#endif
#if defined(__hiuxmpp)
#undef __has_hiux_mpp__
/* Has HI-UX MPP */
#define __has_hiux_mpp__ 1
#endif
#if defined(_hpux) || defined(hpux) || defined(__hpux)
#undef __has_hpux__
/* Has HP-US */
#define __has_hpux__ 1
#endif
#if defined(__OS400__) || defined(__OS400_TGTVRM__)
#undef __has_os_400__
/* Has IBM OS/400 */
#define __has_os_400__ 1
#endif
#if defined(__INTEGRITY)
#undef __has_integrity__
/* Has INTEGRITY */
#define __has_integrity__ 1
#endif
#if defined(__INTERIX)
#undef __has_interix__
/* Has Interix Environment */
#define __has_interix__ 1
#endif
#if defined(sgi) || defined(__sgi)
#undef __has_irix__
/* Has IRIX */
#define __has_irix__ 1
#endif
#if defined(__linux__) || defined(linux) || defined(__linux) || defined(__nividia_fk_you__)
#undef __has_linux__
/* Has Linux */
#define __has_linux__ 1
#endif
#if defined(__Lynx__)
#undef __has_lunx__
/* Has LynxOS */
#define __has_lunx__ 1
#endif
#if defined(macintosh) || defined(Macintosh) || defined(__APPLE__) || defined(__MACH__)
#undef __has_mac_os__
/* Has MacOS */
#define __has_mac_os__ 1
#endif
#if defined(__OS9000) || defined(_OSK)
#undef __has_microware_os_9__
/* Has Microware OS-9 */
#define __has_microware_os_9__ 1
#endif
#if defined(__minix)
#undef __has_minix__
/* Has MINIX */
#define __has_minix__ 1
#endif
#if defined(__MINGW32__) || defined(__MINGW64__)
/* Has MinGW */
#define __has_mingw__ 1
#endif
#if defined(__MORPHOS__)
#undef __has_morph_os__
/* Has MorphOS */
#define __has_morph_os__ 1
#endif
#if defined(mpeix) || defined(__mpexl)
#undef __has_mpe_ix__
/* Has MPE/iX */
#define __has_mpe_ix__ 1
#endif
#if defined(MSDOS) || defined(__MSDOS__) || defined(_MSDOS) || defined(	__DOS__)
#undef __has_dos__
/* Has MSDOS (C++ Plus C unsupport platform) */
#define __has_dos__ 1
#endif
#if defined(__TANDEM)
#undef __has_non_stop__
/* Has NonStop */
#define __has_non_stop__ 1
#endif
#if defined(__nucleus__)
#undef __has_nucleus__
/* Has Nucleus RTOS */
#define __has_nucleus__ 1
#endif
#if defined(OS2) || defined(_OS2) || defined(__OS2__) || defined(__TOS_OS2__)
#undef __has_os2__
/* Has OS/2 */
#define __has_os2__ 1
#endif
#if defined(__palmos__)
#undef __has_palmos__
/* Has Palm OS */
#define __has_palmos__ 1
#endif
#if defined(EPLAN9)
#undef __has_eplan9__
/* Has EPLAN9 */
#define __has_eplan9__ 1
#endif
#if defined(pyr)
#undef __has_pyr__
/* Has Pyramid DC/OSx */
#define __has_pyr__ 1
#endif
#if defined(__QNX__) || defined(__QNXNTO__)
#undef __has_qnx__
/* Has QNX */
#define __has_qnx__ 1
#endif
#if defined(sinux)
#undef __has_reliant_unix__
/* Has Reliant UNIX */
#define __has_reliant_unix__ 1
#endif
#if defined(sun) || defined(__sun)
#undef __has_sun__
/* Has sun */
#define __has_sun__ 1
#if defined(__SVR4) || defined(__svr4__)
#undef __has_solaris__
/* Has Solaris */
#define __has_solaris__ 1
#else
#undef __has_sunos__
/* Has SunOS */
#define __has_sunos__ 1
#endif
#endif
#if defined(__VOS__)
#undef __has_vos__
/* Has Stratus VOS */
#define __has_vos__ 1
#endif
#if defined(__sysv__) || defined(__SVR4) || defined(__svr4__) || defined(_SYSTYPE_SVR4)
#undef __has_svr4__
/* Has SVR4 Environment */
#define __has_svr4__ 1
#endif
#if defined(__SYLLABLE__)
#undef __has_syllable__
/* Has Syllable */
#define __has_syllable__ 1
#endif
#if defined(__SYMBIAN32__)
#undef __has_symbianos__
/* Has Symbian OS */
#define __has_symbianos__ 1
#endif
#if defined(__osf__) || defined(__osf)
#undef __has_ofs1__
/* Has OSF/1 */
#define __has_ofs1__ 1
#endif
#if defined(ultrix) || defined(__ultrix) || defined(__ultrix__) || (defined(unix) && defined(vax))
#undef __has_ultrix__
/* Has Ultrix */
#define __has_ultrix__ 1
#endif
#if defined(_UNICOS)
#undef __has_unicos__
/* Has UNICOS */
#define __has_unicos__ 1
#endif
#if defined(_CRAY) || defined(__crayx1)
#undef __has_unicos_mp__
/* Has UNICOS/mp */
#define __has_unicos_mp__ 1
#endif
#if defined(__unix__) || defined(__unix)
#undef __has_unix__
/* Has UNIX Enviroment */
#define __has_unix__ 1
#endif
#if defined(sco) || defined(_UNIXWARE7)
#undef __has_unix_ware__
/* Has UnixWare */
#define __has_unix_ware__ 1
#endif
#if defined(_UWIN)
#undef __has_uwin__
/* Has U/Win Environment */
#define __has_uwin__ 1
#endif
#if defined(VMS) || defined(__VMS) || defined(__VMS_VER)
#undef __has_vms__
/* Has VMS */
#define __has_vms__ 1
#endif
#if defined(__VXWORKS__) || defined(__vxworks)
#undef __has_vxworks__
/* Has VxWorks */
#define __has_vxworks__ 1
#endif
#if defined(_WIN16) || defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(__TOS_WIN__) || defined(__WINDOWS__)
#undef __has_windows__
/* Has Windows */
#define __has_windows__ 1
#endif
#if defined(_WIN32_WCE)
#undef __has_windows_ce__
/* Has Windows CE */
#define __has_windows_ce__ 1
#endif
#if defined(_WINDU_SOURCE)
#undef __has_windu__
/* Has Wind/U Environment */
#define __has_windu__ 1
#endif
#if defined(__MVS__) || defined(__HOS_MVS__) || defined(__TOS_MVS__)
#undef __has_zos__
/* Has z/OS */
#define __has_zos__ 1
#endif

#endif
