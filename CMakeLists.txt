# Copyright (C) 2023 ChenPi11.
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.17)
project(experiment)

set(CMAKE_C_STANDARD 11)

include_directories(include)

if(WIN32)
    set(PLAT "win32")
else()
    set(PLAT "posix")
endif()

add_executable(server
    "${CMAKE_SOURCE_DIR}/src/buffer.c"
    "${CMAKE_SOURCE_DIR}/src/tcp4_${PLAT}.c"
    "${CMAKE_SOURCE_DIR}/src/tcpex.c"
    "${CMAKE_SOURCE_DIR}/src/readline.c"
    "${CMAKE_SOURCE_DIR}/src/server.c")

add_executable(client
    "${CMAKE_SOURCE_DIR}/src/buffer.c"
    "${CMAKE_SOURCE_DIR}/src/tcp4_${PLAT}.c"
    "${CMAKE_SOURCE_DIR}/src/tcpex.c"
    "${CMAKE_SOURCE_DIR}/src/readline.c"
    "${CMAKE_SOURCE_DIR}/src/client.c")
