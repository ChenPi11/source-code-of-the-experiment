#include <stdio.h>
#include <stdlib.h>

int main()
{
    const char* str1 = "Hello, world!";
    const char* str2 = "你好，世界！";
    const char* str3 = "こんにちは、世界！";
    const char* str4 = "?????, ??!";

    printf("%s\n", str1);
    printf("%s\n", str2);
    printf("%s\n", str3);
    printf("%s\n", str4);

    return EXIT_SUCCESS;
}
