/**
 * @file buffer.c
 * @brief Buffer define and functions.
 * @copyright Copyright (C) 2023 ChenPi11.
 * @date 2023-12-30
 * @version 0.1.0
 * @note This file is part of the charset experiment.
 * @link https://gitlab.com/ChenPi11/source-code-of-the-experiment
 */

/*
 * Copyright (C) 2023 ChenPi11.
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Buffer define and functions.
 */

#include <buffer.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

struct buffer_t alloc_buffer(size_t size)
{
    struct buffer_t buffer;
    buffer.data = malloc(size);
    if (buffer.data == NULL)
    {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    buffer.size = size;
    return buffer;
}

void free_buffer(struct buffer_t* buffer)
{
    free((void*)buffer->data);
    buffer->data = NULL;
    buffer->size = 0;
}

struct buffer_t buffer_from_string(const char* string)
{
    struct buffer_t buffer;
    size_t size = strlen(string);
    buffer.data = malloc(size);
    if (buffer.data == NULL)
    {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    memcpy(buffer.data, string, size);
    buffer.size = size;

    return buffer;
}

void print_buffer(struct buffer_t buffer)
{
    fwrite(buffer.data, buffer.size, 1, stdout);
    fflush(stdout);
}
