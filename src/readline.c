/**
 * @file readline.c
 * @brief Read a line from the standard input.
 * @copyright Copyright (C) 2023 ChenPi11.
 * @date 2023-12-30
 * @version 0.1.0
 * @note This file is part of the charset experiment.
 * @link https://gitlab.com/ChenPi11/source-code-of-the-experiment
 */

/*
 * Copyright (C) 2023 ChenPi11.
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Read a line from the standard input.
 */

#include <readline.h>

#include <stdio.h>
#include <stdlib.h>

#include <buffer.h>

struct buffer_t readline()
{
    uint8_t* buffer = NULL;
    size_t size = 0;
    while(1)
    {
        uint8_t ch = getchar();
        if (ch == '\n')
        {
            break;
        }
        buffer = realloc(buffer, size + 1);
        buffer[size] = ch;
        size++;
    }
    struct buffer_t result = {buffer, size};
    return result;
}
