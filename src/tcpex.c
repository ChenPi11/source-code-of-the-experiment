/**
 * @file tcpex.c
 * @brief TCPEX functions.
 * @copyright Copyright (C) 2023 ChenPi11.
 * @date 2023-12-30
 * @version 0.1.0
 * @note This file is part of the charset experiment.
 * @link https://gitlab.com/ChenPi11/source-code-of-the-experiment
 */

/*
 * Copyright (C) 2023 ChenPi11.
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * TCPEX functions.
 */

#include "buffer.h"
#include <assert.h>
#include <tcpex.h>
#include <tcp4.h>

#include <stdio.h>
#include <string.h>

void sendall(tcp4sock_t sock, struct buffer_t buffer)
{
    size_t sent = 0;

    while (sent < buffer.size)
    {
        struct buffer_t _buffer = {buffer.data + sent, buffer.size - sent};
        ssize_t ret = tcp4_send(sock, _buffer);
        sent += ret;
    }
}

struct buffer_t recvall(tcp4sock_t sock, size_t size)
{
    struct buffer_t buffer = alloc_buffer(size);
    struct buffer_t temp_buffer;
    size_t received = 0;

    while (received < size)
    {
        temp_buffer = tcp4_recv(sock, size - received);
        memcpy(buffer.data + received, temp_buffer.data, temp_buffer.size);
        received += temp_buffer.size;
        free_buffer(&temp_buffer);
    }

    return buffer;
}

/**
 * @brief Send data to a TCPEX socket.
 * @param sock TCPEX socket.
 * @param buffer Buffer.
 */
void tcpex_send(tcp4sock_t sock, struct buffer_t buffer)
{
    struct tcpex_header header = { buffer.size };
    struct buffer_t _buffer = { (uint8_t *)&header, sizeof(struct tcpex_header) };
    sendall(sock, _buffer);
    sendall(sock, buffer);
}

/**
 * @brief Receive data from a TCPEX socket.
 * @param sock TCPEX socket.
 * @return Buffer.
 */
struct buffer_t tcpex_recv(tcp4sock_t sock)
{
    struct tcpex_header header;
    struct buffer_t header_buffer = recvall(sock, sizeof(struct tcpex_header));
    assert(header_buffer.size == sizeof(struct tcpex_header));
    memcpy(&header, header_buffer.data, header_buffer.size);
    struct buffer_t buffer = recvall(sock, header.size);

    return buffer;
}

/**
 * @brief Close a TCPEX socket.
 * @param sock TCPEX socket.
 */
void tcpex_close(tcp4sock_t sock)
{
    shutdown_tcp4(sock);
    close_tcp4(sock);
}
