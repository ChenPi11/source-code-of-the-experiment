/**
 * @file tcp4_win32.c
 * @brief IPv4 TCP socket functions for Win32.
 * @copyright Copyright (C) 2023 ChenPi11.
 * @date 2023-12-30
 * @version 0.1.0
 * @note This file is part of the charset experiment.
 * @link https://gitlab.com/ChenPi11/source-code-of-the-experiment
 */

/*
 * Copyright (C) 2023 ChenPi11.
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * IPv4 TCP socket functions.
 */

#include <string.h>

#include <stdlib.h>
#include <stddef.h>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment(lib, "ws2_32.lib")

#include <buffer.h>
#include <tcp4.h>
#include <cppp/cppp-platform.h>

#if !__has_windows__
#error "This file is only for Win32."
#endif

void perror_win32(const char* msg)
{
    LPSTR message_buffer = NULL;
    FormatMessageA(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        WSAGetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPSTR)&message_buffer,
        0,
        NULL
    );
    fprintf(stderr, "%s: %s\n", msg, message_buffer);
    LocalFree(message_buffer);
}
#define perror perror_win32

int init_tcp4()
{
    WSADATA wsa_data;
    int ret = WSAStartup(MAKEWORD(2, 2), &wsa_data);
    if (ret != 0)
    {
        fprintf(stderr, "WSAStartup failed: %d\n", ret);
        exit(EXIT_FAILURE);
    }
    return ret;
}

tcp4sock_t create_tcp4()
{
    tcp4sock_t sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock == -1)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }
    return sock;
}

void bind_tcp4(tcp4sock_t sock, struct tcp4_addr addr)
{
    struct sockaddr_in sockaddr;
    memset(&sockaddr, 0, sizeof(sockaddr));
    sockaddr.sin_family = AF_INET;
    if (inet_pton(AF_INET, addr.ip, &sockaddr.sin_addr) == -1)
    {
        perror("inet_pton");
        exit(EXIT_FAILURE);
    }
    sockaddr.sin_port = htons(addr.port);
    if (bind(sock, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1)
    {
        perror("bind");
        exit(EXIT_FAILURE);
    }
}

void listen_tcp4(tcp4sock_t sock, int backlog)
{
    if (listen(sock, backlog) == -1)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }
}

tcp4sock_t accept_tcp4(tcp4sock_t sock)
{
    tcp4sock_t accepted_sock = accept(sock, NULL, NULL);
    if (accepted_sock == (tcp4sock_t)(-1))
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    return accepted_sock;
}

void connect_tcp4(tcp4sock_t sock, struct tcp4_addr addr)
{
    struct sockaddr_in sockaddr;
    memset(&sockaddr, 0, sizeof(sockaddr));
    sockaddr.sin_family = AF_INET;
    if (inet_pton(AF_INET, addr.ip, &sockaddr.sin_addr) == -1)
    {
        perror("inet_pton");
        exit(EXIT_FAILURE);
    }
    sockaddr.sin_port = htons(addr.port);
    if (connect(sock, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1)
    {
        perror("connect");
        exit(EXIT_FAILURE);
    }
}

ssize_t tcp4_send(tcp4sock_t sock, struct buffer_t buffer)
{
    ssize_t sent = send(sock, buffer.data, buffer.size, 0);
    if (sent == -1)
    {
        perror("send");
        exit(EXIT_FAILURE);
    }
    return sent;
}

struct buffer_t tcp4_recv(tcp4sock_t sock, size_t size)
{
    char* data = malloc(size);
    ssize_t recv_size = recv(sock, data, size, 0);
    if (recv_size == -1)
    {
        perror("recv");
        exit(EXIT_FAILURE);
    }
    struct buffer_t buffer = alloc_buffer(recv_size);
    memcpy(buffer.data, data, recv_size);
    free(data);

    return buffer;
}

void shutdown_tcp4(tcp4sock_t sock)
{
    if (shutdown(sock, 2) == -1)
    {
        perror("shutdown");
    }
}

void close_tcp4(tcp4sock_t sock)
{
    if (close(sock) == -1)
    {
        perror("close");
        exit(EXIT_FAILURE);
    }
}
