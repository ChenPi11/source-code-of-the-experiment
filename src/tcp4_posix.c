/**
 * @file tcp4_posix.c
 * @brief IPv4 TCP socket functions for POSIX.
 * @copyright Copyright (C) 2023 ChenPi11.
 * @date 2023-12-30
 * @version 0.1.0
 * @note This file is part of the charset experiment.
 * @link https://gitlab.com/ChenPi11/source-code-of-the-experiment
 */

/*
 * Copyright (C) 2023 ChenPi11.
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * IPv4 TCP socket functions.
 */

#include <string.h>
#include <tcp4.h>

#include <stdlib.h>
#include <stddef.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <buffer.h>
#include <cppp/cppp-platform.h>

#if !__has_unix__ || !__has_linux__
#error "This file is only for POSIX."
#endif

int init_tcp4()
{
    return 0;
}

tcp4sock_t create_tcp4()
{
    tcp4sock_t sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock == -1)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }
    return sock;
}

void bind_tcp4(tcp4sock_t sock, struct tcp4_addr addr)
{
    struct sockaddr_in sockaddr;
    bzero(&sockaddr, sizeof(sockaddr));
    sockaddr.sin_family = AF_INET;
    if (inet_pton(AF_INET, addr.ip, &sockaddr.sin_addr) == -1)
    {
        perror("inet_pton");
        exit(EXIT_FAILURE);
    }
    sockaddr.sin_port = htons(addr.port);
    if (bind(sock, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1)
    {
        perror("bind");
        exit(EXIT_FAILURE);
    }
}

void listen_tcp4(tcp4sock_t sock, int backlog)
{
    if (listen(sock, backlog) == -1)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }
}

tcp4sock_t accept_tcp4(tcp4sock_t sock)
{
    tcp4sock_t accepted_sock = accept(sock, NULL, NULL);
    if (accepted_sock == (tcp4sock_t)(-1))
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    return accepted_sock;
}

void connect_tcp4(tcp4sock_t sock, struct tcp4_addr addr)
{
    struct sockaddr_in sockaddr;
    bzero(&sockaddr, sizeof(sockaddr));
    sockaddr.sin_family = AF_INET;
    if (inet_pton(AF_INET, addr.ip, &sockaddr.sin_addr) == -1)
    {
        perror("inet_pton");
        exit(EXIT_FAILURE);
    }
    sockaddr.sin_port = htons(addr.port);
    if (connect(sock, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1)
    {
        perror("connect");
        exit(EXIT_FAILURE);
    }
}

ssize_t tcp4_send(tcp4sock_t sock, struct buffer_t buffer)
{
    ssize_t sent = send(sock, buffer.data, buffer.size, 0);
    if (sent == -1)
    {
        perror("send");
        exit(EXIT_FAILURE);
    }
    return sent;
}

struct buffer_t tcp4_recv(tcp4sock_t sock, size_t size)
{
    char* data = malloc(size);
    ssize_t recv_size = recv(sock, data, size, 0);
    if (recv_size == -1)
    {
        perror("recv");
        exit(EXIT_FAILURE);
    }
    struct buffer_t buffer = alloc_buffer(recv_size);
    memcpy(buffer.data, data, recv_size);
    free(data);

    return buffer;
}

void shutdown_tcp4(tcp4sock_t sock)
{
    if (shutdown(sock, SHUT_RDWR) == -1)
    {
        perror("shutdown");
        exit(EXIT_FAILURE);
    }
}

void close_tcp4(tcp4sock_t sock)
{
    if (close(sock) == -1)
    {
        perror("close");
        exit(EXIT_FAILURE);
    }
}
