/**
 * @file client.c
 * @brief Chat client.
 * @copyright Copyright (C) 2023 ChenPi11.
 * @date 2023-12-30
 * @version 0.1.0
 * @note This file is part of the charset experiment.
 * @link https://gitlab.com/ChenPi11/source-code-of-the-experiment
 */

/*
 * Copyright (C) 2023 ChenPi11.
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Chat program client (P2P).
 */

#include <stdio.h>
#include <string.h>

#include <buffer.h>
#include <readline.h>
#include <tcp4.h>
#include <tcpex.h>

struct buffer_t buffer;

int main()
{
    init_tcp4();
    tcp4sock_t sock = create_tcp4();
    struct tcp4_addr addr = { "127.0.0.1", 1234 };
    connect_tcp4(sock, addr);

    printf("Connected.\n");
    while(1)
    {
        buffer = tcpex_recv(sock);
        printf("Peer: ");
        print_buffer(buffer);
        free_buffer(&buffer);

        printf("\nYou: ");
        buffer = readline();
        if (strcmp((char*)buffer.data, "bye\n") == 0)
        {
            free_buffer(&buffer);
            break;
        }
        tcpex_send(sock, buffer);
        free_buffer(&buffer);
    }

    tcpex_close(sock);

    return 0;
}
